import { Grid } from "@material-ui/core";
import styles from "./App.module.scss";
import Balance from "./components/Balance/Balance.component";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary.component";
import Filter from "./components/Filter/Filter.component";
import Footer from "./components/Footer.component";
import Header from "./components/Header.component";
import TransactionForm from "./components/TransactionForm/TransactionForm.component";
import TransactionsList from "./components/TransactionsList/TransactionsList.component";

const App: React.FC = () => {
  return (
    <div className={styles.application}>
      <div className={styles.header}>
        <Header />
      </div>
      <div className={styles.content}>
        <Grid item xs={12} md={8}>
          <div className={styles.mainPanel}>
            <div className={styles.head}>
              <Grid container className={styles.container}>
                <Grid item xs={12} md={6} className={styles.tools}
                >
                  <div className={styles.balance}>
                    <Balance />
                  </div>
                  <div className={styles.filter}>
                    <ErrorBoundary>
                      <Filter />
                    </ErrorBoundary>
                  </div>
                </Grid>
                <Grid item xs={12} md={6} className={styles.form}>
                  <TransactionForm />
                </Grid>
              </Grid>
            </div>
            <div className={styles.transactions}>
              <ErrorBoundary>
                <TransactionsList />
              </ErrorBoundary>
            </div>
          </div>
        </Grid>
      </div>
      <div className={styles.footer}>
       <Footer/>
      </div>
    </div >
  );
}

export default App;
