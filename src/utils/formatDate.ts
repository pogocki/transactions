export const getFormattedDate = (date: string): string => {
  const dateObject = new Date(date);
  const dateFormatted = dateObject.toLocaleDateString("pl-PL", { year: "numeric", month: "long", day: "numeric" });
  const timeFormatted = dateObject.toLocaleTimeString("pl-PL", { hour: "2-digit", minute: "2-digit" });
  return `${dateFormatted} - ${timeFormatted}`;
};
