import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../store/store";
import { transactionsActions } from "../store/transactionsSlice";

export const ResultsDisplay: React.FC = () => {
    const dispatch = useDispatch<AppDispatch>();
    const currentPageTransactions = useSelector((state: RootState) => state.transactions.currentPageTransactions);
    useEffect(() => {
        dispatch(transactionsActions.loadAllTranslations());
    }, []);
    return <>
        {currentPageTransactions.map(item => (
            <div key={item.id}>
                <h1>{item.beneficiary}</h1>
            </div>))}
    </>
}