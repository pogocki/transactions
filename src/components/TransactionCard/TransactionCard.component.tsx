import { Button, Card, CardActions, CardContent, Slide, Typography } from "@material-ui/core";
import { getFormattedDate } from "../../utils/formatDate";
import styles from "./TransactionCard.module.scss";
import { ITransaction } from "../../models/transaction";
import { transactionsActions } from "../../store/transactionsSlice";
import { useDispatch } from "react-redux";
import { useState } from "react";

interface TransactionCardProps {
    transaction: ITransaction;
}

const strings = {
    dolar: '$',
    buttons: {
        remove: 'Remove transaction'
    }
}

const TransactionCard: React.FC<TransactionCardProps> = ({ transaction, }) => {
    const dispatch = useDispatch();
    const [inState, setInState] = useState<boolean>(true);
    const handleDeleteTransaction = () => dispatch(transactionsActions.removeTransaction(transaction.id));

    return (
        <>
            <Slide direction="up" in={inState} mountOnEnter unmountOnExit appear
                onExited={handleDeleteTransaction}
            >
                <Card key={transaction.id} className={styles.transactionCard}>
                    <CardContent>
                        <div className={styles.transactionCard}>
                            <div className={styles.mainRow}>
                                <div className={styles.amountContainer}>
                                    <Typography>
                                        <span>{strings.dolar}</span> {transaction.amount}
                                    </Typography>
                                </div>
                                <div className={styles.beneficiaryContainer}>
                                    <Typography>
                                        {transaction.beneficiary}
                                    </Typography>
                                    <Typography className={styles.account}>
                                        {transaction.account}
                                    </Typography>
                                    <Typography variant="body2" component="p">
                                        {transaction.address}
                                    </Typography>
                                </div>
                                <div className={styles.dateDescrContainer}>
                                    <Typography className={styles.date}>
                                        {getFormattedDate(transaction.date)}
                                    </Typography>
                                    <Typography variant="body2">
                                        {transaction.description}
                                    </Typography>
                                </div>
                            </div>
                        </div>
                    </CardContent>
                    <CardActions className={styles.actionsRow}>
                        <Button size="small" variant="contained" color="primary" onClick={() => setInState(false)}>{strings.buttons.remove}</Button>
                    </CardActions>
                </Card>
            </Slide>
        </>
    )
}

export default TransactionCard;