import { Typography } from "@material-ui/core";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";
import styles from "./Balance.module.scss";

const Balance: React.FC = () => {

    const balance = useSelector((state: RootState) => state.transactions.balance);

    return (
        <div className={styles.balancePanel}>
            <Typography>
                {balance.toFixed(2)}
            </Typography>
        </div>
    )
}

export default Balance;