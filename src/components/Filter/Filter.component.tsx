import { TextField } from "@material-ui/core";
import _ from "lodash";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/store";
import { transactionsActions } from "../../store/transactionsSlice";

const Filter: React.FC = () => {

    const searchPhase = useSelector((state: RootState) => state.transactions.searchPhase);
    const dispatch = useDispatch();

    const [enteredPhase, setEnteredPhase] = useState<string>(searchPhase);

    const handleSearchPhaseChangeGlobal = _.debounce((newPhase: string) => {
        dispatch(transactionsActions.updateSearchPhase(newPhase))
    }, 500);

    const handleSearchPhaseChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setEnteredPhase(event.target.value);
        handleSearchPhaseChangeGlobal(event.target.value);
    };

    return (<>
        <TextField label="Search" type="search" variant="outlined"
            value={enteredPhase}
            onChange={handleSearchPhaseChange}
            inputProps={{ ['data-testid']: 'filter_input' }}

        />
    </>)
}

export default Filter;