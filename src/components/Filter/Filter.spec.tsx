import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { useEffect } from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState, store } from '../../store/store';
import { transactionsActions } from '../../store/transactionsSlice';
import { ResultsDisplay } from '../../utils/testHelper';
import Filter from './Filter.component';

const mockedTransactions = [
    {
        id: 0,
        amount: -2008.75,
        beneficiary: "Jan Kowalski",
        account: "PL10104092290785174000000000",
        address: "185 Berkeley Place, Brady, West Virginia, 7409",
        date: "2021-12-15T01:05:42",
        description: "Amet amet qui proident sint esse adipisicing amet."
    },
    {
        id: 1,
        amount: -2038.61,
        beneficiary: "Amie Whitley",
        account: "PL10103486643679406000000000",
        address: "827 Dahl Court, Stagecoach, Louisiana, 3343",
        date: "2019-12-12T06:58:38",
        description: "Occaecat nulla Lorem id ullamco."
    },
    {
        id: 2,
        amount: 1050.01,
        beneficiary: "John Nintendo",
        account: "PL10103486643672306000000000",
        address: "827 Dahl Court, Stagecoach, Louisiana, 3343",
        date: "2019-12-12T06:58:38",
        description: "Occaecat nulla Lorem id ullamco."
    }
];

jest.mock("axios", () => {
    return {
        get: () => {
            return Promise.resolve({
                data: mockedTransactions,
                status: 200,
                statusText: "OK",
            })
        }
    }
});

describe('Filter', () => {

    it('renders component', async () => {
        render(<Provider store={store}>
            <Filter />
            <ResultsDisplay />
        </Provider>);

        await waitFor(() => expect(screen.queryByText('Jan Kowalski')).toBeInTheDocument());
        const filterInput = screen.getByTestId('filter_input');
        fireEvent.change(
            filterInput,
            { target: { value: 'Nin' } },
        );
        await waitFor(() => expect(screen.queryByText('Jan Kowalski')).not.toBeInTheDocument());
        await waitFor(() => expect(screen.getByText('John Nintendo')).toBeInTheDocument());
    })

})
