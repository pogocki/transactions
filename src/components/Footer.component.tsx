import { Toolbar, Typography } from "@material-ui/core";

const Footer: React.FC = () => {

    return (
        <Toolbar>
            <Typography
                variant="h6"
            >
                FOOTER
            </Typography>
        </Toolbar>
    )
}

export default Footer;