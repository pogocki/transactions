import { AppBar, Toolbar, Typography } from "@material-ui/core";

const Header: React.FC = () => {

    return (
        <AppBar component="nav">
            <Toolbar>
                <Typography
                    variant="h6"
                >
                    HEADER
                </Typography>
            </Toolbar>
        </AppBar>
    )
}

export default Header;