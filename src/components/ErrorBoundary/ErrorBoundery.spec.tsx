import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { Exception } from 'sass';
import ErrorBoundary from './ErrorBoundary.component';


const ComponentWithoutError: React.FC = () => {
    return <>Ala ma kota</>
}

const ComponentWithError: React.FC = () => {
    throw new Error('error');
    return <>Ala ma kota</>
}


describe('ErrorBoundary', () => {

    it('renders component without error', async () => {
        render(<ErrorBoundary><ComponentWithoutError /></ErrorBoundary>);
        expect(screen.getByText("Ala ma kota")).toBeInTheDocument();
    })

    it('renders component with error', async () => {
        try {
            render(<ErrorBoundary><ComponentWithError /></ErrorBoundary>);
        } catch (error) {
            console.log(error);
        }

        expect(screen.queryByText("Ala ma kota")).not.toBeInTheDocument();
        expect(screen.getByText("Something went wrong")).toBeInTheDocument();
    })

})
