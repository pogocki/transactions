import React, { ErrorInfo, ReactNode } from "react";
import styles from './ErrorBoundary.module.scss';
import { Alert } from "@mui/material";

const strings = { error: 'Something went wrong' }

interface ErrorBoundaryProps {
    children?: ReactNode;
}

interface ErrorBoundaryState {
    hasError: boolean;
}

class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
    public state: ErrorBoundaryState = {
        hasError: false
    };

    public static getDerivedStateFromError(): ErrorBoundaryState {
        return { hasError: true };
    }

    public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.error("Error:", error, errorInfo);
    }

    public render() {
        if (this.state.hasError) {
            return (
                <div className={styles.errorBoundary}>
                    <div>
                        <Alert severity="error">{strings.error}</Alert>
                    </div>
                </div>
            );
        }

        return this.props.children;
    }
}

export default ErrorBoundary;