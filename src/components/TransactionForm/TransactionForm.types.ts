export interface TransactionFormValues {
  [field: string]: string;
  amount: string;
  accountNumber: string;
  address: string;
  description: string;
  beneficiary: string;
}
