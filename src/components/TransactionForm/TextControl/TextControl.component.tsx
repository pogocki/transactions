import { TextField } from "@material-ui/core";
import { useFormik } from "formik";
import { TransactionFormValues } from "../TransactionForm.types";

interface TextControlProps {
    formik: Pick<ReturnType<typeof useFormik<Partial<TransactionFormValues>>>, "values" | "handleChange" | "touched" | "errors">;
    fieldName: string;
    label: string;
}

const TextControl: React.FC<TextControlProps> = ({ formik, fieldName, label }) => {
    return (
        <TextField
            variant="outlined"
            size="small"
            id={fieldName}
            name={fieldName}
            label={label}
            value={formik.values[fieldName]}
            onChange={formik.handleChange}
            error={formik.touched[fieldName] && !!formik.errors[fieldName]}
            helperText={formik.touched[fieldName] && formik.errors[fieldName]}
            margin="dense"
        />
    )
}

export default TextControl;