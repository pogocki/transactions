import { render, screen, } from '@testing-library/react';
import TextControl from './TextControl.component';

const mockFormik = {
    values: { account: 'Abrakadabra' },
    handleChange: jest.fn,
    touched: { account: false },
    errors: {}
}

describe('TextControl', () => {

    it('renders component attributes', async () => {
        render(
            <TextControl
                formik={mockFormik}
                fieldName="account"
                label="Label of the property"
            />);
        expect(screen.getByDisplayValue("Abrakadabra")).toBeInTheDocument();
        expect(screen.getByLabelText("Label of the property")).toBeInTheDocument();
    })
})
