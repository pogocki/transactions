import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import TransactionForm from './TransactionForm.component';
import { Provider, useDispatch } from 'react-redux';
import { store } from '../../store/store';
import { ResultsDisplay } from '../../utils/testHelper';


const mockedTransactions = [
    {
        id: 0,
        amount: -2008.75,
        beneficiary: "Jan Kowalski",
        account: "PL10104092290785174000000000",
        address: "185 Berkeley Place, Brady, West Virginia, 7409",
        date: "2021-12-15T01:05:42",
        description: "Amet amet qui proident sint esse adipisicing amet."
    },
    {
        id: 1,
        amount: -2038.61,
        beneficiary: "Amie Whitley",
        account: "PL10103486643679406000000000",
        address: "827 Dahl Court, Stagecoach, Louisiana, 3343",
        date: "2019-12-12T06:58:38",
        description: "Occaecat nulla Lorem id ullamco."
    },
    {
        id: 2,
        amount: 1050.01,
        beneficiary: "John Nintendo",
        account: "PL10103486643672306000000000",
        address: "827 Dahl Court, Stagecoach, Louisiana, 3343",
        date: "2019-12-12T06:58:38",
        description: "Occaecat nulla Lorem id ullamco."
    }
];

jest.mock("axios", () => {
    return {
        get: () => {
            return Promise.resolve({
                data: mockedTransactions,
                status: 200,
                statusText: "OK",
            })
        }
    }
});




describe('TransactionForm', () => {

    it('renders all fields', async () => {
        render(<Provider store={store}><TransactionForm /></Provider>);
        expect(screen.getByLabelText("Amount")).toBeInTheDocument();
        expect(screen.getByLabelText("Account Number")).toBeInTheDocument();
        expect(screen.getByLabelText("Address")).toBeInTheDocument();
        expect(screen.getByLabelText("Description")).toBeInTheDocument();
        expect(screen.getByLabelText("Beneficiary")).toBeInTheDocument();
    })

    it('renders warning for empty Amount value', async () => {
        render(
            <Provider store={store}>
                <TransactionForm />
                {/* <ResultsDisplay /> */}
            </Provider>);

        const submitButton = screen.getByText("Add transaction").closest('button');
        fireEvent(
            submitButton!,
            new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
            }),
        );

        await waitFor(() => expect(screen.getByText('Amount is required')).toBeInTheDocument());
    })

    it('renders warning for negative Amount value', async () => {
        render(
            <Provider store={store}>
                <TransactionForm />
                {/* <ResultsDisplay /> */}
            </Provider>);

        const amountInput = screen.getByLabelText("Amount");
        fireEvent.change(
            amountInput,
            { target: { value: '-997' } },
        );

        const submitButton = screen.getByText("Add transaction").closest('button');
        fireEvent(
            submitButton!,
            new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
            }),
        );

        await waitFor(() => expect(screen.getByText('Amount must be positive')).toBeInTheDocument());
    })

    it('renders warning for non-digit Amount value', async () => {
        render(
            <Provider store={store}>
                <TransactionForm />
                {/* <ResultsDisplay /> */}
            </Provider>);

        const amountInput = screen.getByLabelText("Amount");
        fireEvent.change(
            amountInput,
            { target: { value: 'Wodogrzmoty Mickiewicza' } },
        );

        const submitButton = screen.getByText("Add transaction").closest('button');
        fireEvent(
            submitButton!,
            new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
            }),
        );

        await waitFor(() => expect(screen.getByText('Amount must be a number')).toBeInTheDocument());
    })

    it('renders new transaction row after submitting', async () => {
        render(
            <Provider store={store}>
                <TransactionForm />
                <ResultsDisplay />
            </Provider>);

        fireEvent.change(
            screen.getByLabelText("Amount"),
            { target: { value: '997' } },
        );

        fireEvent.change(
            screen.getByLabelText("Account Number"),
            { target: { value: '1111111111' } },
        );

        fireEvent.change(
            screen.getByLabelText("Address"),
            { target: { value: 'Wiejska 3/4' } },
        );

        fireEvent.change(
            screen.getByLabelText("Description"),
            { target: { value: 'Ala ma kota' } },
        );

        fireEvent.change(
            screen.getByLabelText("Beneficiary"),
            { target: { value: 'Bajlando bajlando' } },
        );

        const submitButton = screen.getByText("Add transaction").closest('button');
        fireEvent(
            submitButton!,
            new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
            }),
        );

        await waitFor(() => expect(screen.getByText('Bajlando bajlando')).toBeInTheDocument());
    })
})
