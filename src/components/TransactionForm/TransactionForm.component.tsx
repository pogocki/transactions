import { Button, Snackbar, } from "@material-ui/core";
import { Alert, AlertColor } from "@mui/material";
import { useFormik } from "formik";
import { useState } from "react";
import { useDispatch } from "react-redux";
import * as yup from 'yup';
import { transactionsActions } from "../../store/transactionsSlice";
import TextControl from "./TextControl/TextControl.component";
import styles from './TransactionForm.module.scss';
import { TransactionFormValues } from "./TransactionForm.types";

const strings = {
    fields: {
        amount: 'Amount',
        accountNumber: 'Account Number',
        address: 'Address',
        description: 'Description',
        beneficiary: 'Beneficiary'
    },
    addTransaction: 'Add transaction',
    successMsg: 'Done'
}


const TransactionForm: React.FC = () => {

    const dispatch = useDispatch();

    const [alert, setAlert] = useState<{ severity: AlertColor, message: string } | null>(null);


    const validationSchema = yup.object({
        amount: yup
            .number()
            .typeError('Amount must be a number')
            .positive('Amount must be positive')
            .required('Amount is required'),
        accountNumber: yup
            .string()
            .matches(/^[0-9]+/, 'Account number should containg only digits')
            .required('Account number is required'),
        address: yup
            .string()
            .min(10, 'Account number should be at least 10 characters long')
            .required('Address number is required'),
        description: yup
            .string()
            .required('Description is required'),
        beneficiary: yup
            .string()
            .required('Beneficiary is required'),
    });

    const formik = useFormik<TransactionFormValues>({
        initialValues: {
            amount: '',
            accountNumber: '',
            address: '',
            description: '',
            beneficiary: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values, { resetForm }) => {
            dispatch(transactionsActions.addTransaction({
                id: Math.round(Math.random() * 1000),
                amount: parseInt(values.amount),
                beneficiary: values.beneficiary,
                account: values.accountNumber,
                address: values.address,
                date: new Date().toString(),
                description: values.description,
            }));
            resetForm();
            setAlert({ severity: 'success', message: strings.successMsg });
        },
    });

    return (
        <>
            <div className={styles.transactionForm}>
                <form onSubmit={formik.handleSubmit}>
                    <div>
                        <TextControl
                            formik={formik}
                            fieldName="amount"
                            label={strings.fields.amount}
                        />
                    </div>
                    <div>
                        <TextControl
                            formik={formik}
                            fieldName="accountNumber"
                            label={strings.fields.accountNumber}
                        />
                    </div>
                    <div>
                        <TextControl
                            formik={formik}
                            fieldName="address"
                            label={strings.fields.address}
                        />
                    </div>
                    <div>
                        <TextControl
                            formik={formik}
                            fieldName="description"
                            label={strings.fields.description}
                        />
                    </div>
                    <div>
                        <TextControl
                            formik={formik}
                            fieldName="beneficiary"
                            label={strings.fields.beneficiary}
                        />
                    </div>
                    <div>
                        <Button color="primary" variant="contained" type="submit">
                            {strings.addTransaction}
                        </Button>
                    </div>
                </form>
            </div>
            {alert && (
                <Snackbar open={true} autoHideDuration={6000} onClose={() => setAlert(null)}
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                >
                    <Alert onClose={() => setAlert(null)} severity={alert.severity}>
                        {alert.message}
                    </Alert>
                </Snackbar>)
            }
        </>
    )
}

export default TransactionForm;