import { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import styles from "./TransactionsList.module.scss";
import { AppDispatch, RootState } from "../../store/store";
import { transactionsActions } from "../../store/transactionsSlice";
import { Pagination } from "@mui/material";
import { itemsPerPageCount } from "../../consts/pagination";
import TransactionCard from "../TransactionCard/TransactionCard.component";

const TransactionsList: React.FC = () => {
    const currentPageTransactions = useSelector((state: RootState) => state.transactions.currentPageTransactions);
    const allTranslationsLength = useSelector((state: RootState) => state.transactions.allTransactions.length);
    const dispatch = useDispatch<AppDispatch>();

    const handleChangePagination = (event: React.ChangeEvent<unknown>, page: number) => {
        dispatch(transactionsActions.goToPageOfTransactions(page));
    }

    useEffect(() => {
        dispatch(transactionsActions.loadAllTranslations());
    }, [dispatch]);

    return (<>
        <div className={styles.transactionsPanel}>
            <Pagination count={Math.round(allTranslationsLength / itemsPerPageCount)} onChange={handleChangePagination} />
            <div className={styles.transactionsList}>
                {currentPageTransactions.map(transaction => (
                    <TransactionCard key={transaction.id} transaction={transaction} />
                ))}
            </div>
        </div>
    </>)
}

export default TransactionsList;