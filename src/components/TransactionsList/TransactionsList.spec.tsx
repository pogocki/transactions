import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import TransactionsList from './TransactionsList.component';
import { Provider } from 'react-redux';
import { store } from '../../store/store';


const mockedTransactions = [
    {
        id: 0,
        amount: -2008.75,
        beneficiary: "Jan Kowalski",
        account: "PL10104092290785174000000000",
        address: "185 Berkeley Place, Brady, West Virginia, 7409",
        date: "2021-12-15T01:05:42",
        description: "Amet amet qui proident sint esse adipisicing amet."
    },
    {
        id: 1,
        amount: -2038.61,
        beneficiary: "Amie Whitley",
        account: "PL10103486643679406000000000",
        address: "827 Dahl Court, Stagecoach, Louisiana, 3343",
        date: "2019-12-12T06:58:38",
        description: "Occaecat nulla Lorem id ullamco."
    },
    {
        id: 2,
        amount: 1050.01,
        beneficiary: "John Nintendo",
        account: "PL10103486643672306000000000",
        address: "827 Dahl Court, Stagecoach, Louisiana, 3343",
        date: "2019-12-12T06:58:38",
        description: "Occaecat nulla Lorem id ullamco."
    }
];

jest.mock("axios", () => {
    return {
        get: () => {
            return Promise.resolve({
                data: mockedTransactions,
                status: 200,
                statusText: "OK",
            })
        }
    }
});

jest.mock("../../consts/pagination", () => {
    return {
        itemsPerPageCount: 2
    }
})

describe('TransactionsList', () => {

    it('renders component with data', async () => {
        render(<Provider store={store}><TransactionsList /></Provider>);
        await waitFor(() => expect(screen.getByText('Jan Kowalski')).toBeInTheDocument());
        expect(screen.getByText("PL10104092290785174000000000")).toBeInTheDocument();
        expect(screen.getByText("185 Berkeley Place, Brady, West Virginia, 7409")).toBeInTheDocument();
        expect(screen.getByText("Amet amet qui proident sint esse adipisicing amet.")).toBeInTheDocument();
    })

    it('renders second page after clicking pagination step 2', async () => {
        render(<Provider store={store}><TransactionsList /></Provider>);

        const step2Button = screen.getByLabelText('Go to page 2');
        fireEvent(
            step2Button,
            new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
            }),
        );

        await waitFor(() => expect(screen.getByText('John Nintendo')).toBeInTheDocument());
    })
})
