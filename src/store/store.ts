import { AnyAction, configureStore } from "@reduxjs/toolkit";
import { ThunkDispatch } from "redux-thunk";
import transactionsSlice from "./transactionsSlice";

export const store = configureStore({
  reducer: {
    transactions: transactionsSlice,
  }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch & ThunkDispatch<RootState, unknown, AnyAction>;
