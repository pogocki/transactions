import { createSlice } from "@reduxjs/toolkit";
import { ITransaction } from "../models/transaction";
import { getTransactions } from "../services/transactions.service";
import { AppDispatch } from "./store";
import { itemsPerPageCount } from "../consts/pagination";

interface ITransactionsState {
  allTransactions: ITransaction[];
  currentPageTransactions: ITransaction[];
  balance: number;
  searchPhase: string;
}

const initialState: ITransactionsState = {
  allTransactions: [],
  currentPageTransactions: [],
  balance: 0,
  searchPhase: "",
};

const getFilteredTransactions = (state: ITransactionsState, searchPhase: string) => {
  const lowerCaseSearchPhase = searchPhase.toLocaleLowerCase();

  return searchPhase
    ? state.allTransactions.filter((item) => item.beneficiary.toLocaleLowerCase().includes(lowerCaseSearchPhase))
    : state.allTransactions;
};

const getBalance = (allTransactions: ITransaction[]) => {
  return allTransactions.reduce((sum: number, nextTransaction: ITransaction) => {
    return sum + nextTransaction.amount;
  }, 0);
};

export const transactionsSlice = createSlice({
  name: "transactions",
  initialState,
  reducers: {
    initAllTransactions: (state, action: { type: string; payload: ITransaction[] }) => {
      return {
        ...state,
        allTransactions: [...action.payload],
        balance: getBalance(action.payload),
        currentPageTransactions: action.payload.slice(0, itemsPerPageCount),
      };
    },
    goToPageOfTransactions: (state, action: { type: string; payload: number }) => {
      const startIndex = (action.payload - 1) * itemsPerPageCount;
      return {
        ...state,
        currentPageTransactions: getFilteredTransactions(state, state.searchPhase).slice(
          startIndex,
          startIndex + itemsPerPageCount
        ),
      };
    },
    updateSearchPhase: (state, action: { type: string; payload: string }) => {
      return {
        ...state,
        searchPhase: action.payload,
        currentPageTransactions: getFilteredTransactions(state, action.payload).slice(0, itemsPerPageCount),
      };
    },
    addTransaction: (state, action: { type: string; payload: ITransaction }) => {
      const allTransactions = [action.payload, ...state.allTransactions];
      return {
        ...state,
        allTransactions,
        currentPageTransactions: [action.payload, ...state.currentPageTransactions],
        balance: getBalance(allTransactions),
        searchPhase: "",
      };
    },
    removeTransaction: (state, action: { type: string; payload: number }) => {
      const allTransactions = state.allTransactions.filter((item) => item.id !== action.payload);
      return {
        ...state,
        allTransactions,
        balance: getBalance(allTransactions),
        currentPageTransactions: state.currentPageTransactions.filter((item) => item.id !== action.payload),
      };
    },
  },
});

const loadAllTranslations = () => {
  return (dispatch: AppDispatch) => {
    getTransactions().then((response) => {
      dispatch(transactionsSlice.actions.initAllTransactions(response.data));
    });
  };
};

export const transactionsActions = { ...transactionsSlice.actions, loadAllTranslations };
export default transactionsSlice.reducer;
