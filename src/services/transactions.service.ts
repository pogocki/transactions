import { ITransaction } from "../models/transaction";
import axios, { AxiosResponse } from "axios";

export const getTransactions = (): Promise<AxiosResponse<ITransaction[]>> => {
  return axios.get("http://localhost:3004/transactions");
};
